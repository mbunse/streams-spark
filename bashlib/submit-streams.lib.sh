# 
# streams-spark - an Apache Spark extension for the streams framework
# 
# Copyright (C) 2016 by the participants of the Project Group 594 at
# the University of Technology in Dortmund: Mohamed Asmi, Alexander
# Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
# Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
# Carolin Wiethoff and Lili Xu.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
# 


CONF_FILE="conf/submit-streams.conf.sh"
MAIN_ARTIFACT="de.sfb876:streams-spark:0.0.2-SNAPSHOT:jar"


# Man page (shown with --help)
function showManpage() {
	cat <<- _EOF_

		  This script is used to submit a Spark job to YARN that executes the given XML configuration
		  using the Streams extension developed by the PG 594 at the CRC 876.
		  The XML configuration has to reside on the HDFS. It is specified relative to the HDFS base
		  URL (see example). To upload a local XML beforehand, use the --local option.

		  Usage: ./submit-streams.sh [options] <xml file>

		  Options:
		      --job-name STRING           The name of the job that is displayed in YARN (Default is
		                                  'streams-spark')
		      --stream                    If this flag is provided, a Spark Streaming job is started
		                                  instead of a batch job.
		      --maxemptyrdds NUM          Number of empty RDDs to be seen before a Streaming job
		                                  terminates (Default: 60). Does not apply to batch jobs.
		      --numreceivers NUM          Number of receivers to use. Determines parallelization level.
		                                  If the default value of -1 is provided, this number will be
		                                  set to the number of executors. Does not apply to batch jobs.
		      --block-interval NUMms      Time in milliseconds (other units can be used, as well)
		                                  for Spark Streaming to chop streams into mini-batch blocks.
		                                  Does not apply to batch jobs.
		      --driver-cores NUM          Number of cores used by the driver (Default: 2).
		      --driver-memory NUMg        Gigabytes of memory used by driver (Default: 8g).
		      --maxresultsize NUMg        Maximum size of results to be collected. Should be less than
		                                  or equal to driver memory (Default: 8g).
		      --num-executors NUM         Number of executors (= JVMs) to launch (Default: 2),
		                                  determining the degree of parallelization.
		      --executor-cores NUM        Number of cores per executor (Default: 2). It is advised
		                                  to keep the default.
		      --executor-memory NUMg      Gigabytes of memory to use in executors (Default: 4g).
		      --wait                      Do not exit when the app is accepted but wait for it to
		                                  finish.
		      --local FILENAME            Upload the given local file to HDFS. The <xml file> argu-
		                                  ment now specifies the target path. If there already is a
		                                  file at this location, the file is overwritten.
		      --help                      Show this page.

		  Example:
		      ./submit-streams.sh  --num-executors 4  /streams-spark/batchProcessExample.xml

		  Further Instructions:
		      Make sure, that the following environment variables are set (using export):
		      - HADOOP_HOME (e.g., '/usr/local/hadoop-2.7.0/')
		      - SPARK_HOME (e.g., '/usr/local/spark-2.1.0/')
		      - HADOOP_USER_NAME (e.g., 'bunse'; 'hadoop' is not encouraged)
		      Also make sure, that on the given paths Hadoop and Spark are actually installed.

	_EOF_
}


# Searches the hadoop configuration for the HDFS root URL. Then echoes it.
function getDefaultFS() {

	read_dom () { local IFS=\>; read -d \< ENTITY CONTENT; }	# helper reading XML <tag>value</tag> pairs

	# read out all config parameters and parse them
	${HADOOP_HOME}/bin/hadoop org.apache.hadoop.conf.Configuration | while read cline; do
		propertyFound="false"	# look for property fs.defaultFS

		# interpret this line of config output
		echo ${cline} | while read_dom; do 	# iterate over DOM elements of this line

			# first, find a name entity with value fs.defaultFS
		    if [[ $ENTITY = "name" && $CONTENT = "fs.defaultFS" ]]; then
		    	propertyFound="true"

		    # then, look for the value entity and read its content
		    elif [[ $propertyFound = "true" && $ENTITY = "value" ]]; then
		    	echo $CONTENT 	# this is the value of the property fs.defaultFS
		    	exit
		    fi

		done
	done

}


# Check properties and environment variables to be set correctly
function checkProperties() {

	_e=0

	# check that properties are read correctly
	if [ -z ${DEPENDENCY_JARS} ]; then
		echo "ERROR: Property DEPENDENCY_JARS was not specified" >&2
		_e=1
	fi
	if [ -z ${EVENTLOG_DIR} ]; then
		echo "ERROR: Property EVENTLOG_DIR was not specified" >&2
		_e=1
	fi
	if [ ${_e} -ne 0 ]; then
		exit ${_e}
	fi

	# additional properties that may reside in properties file or be exported
	if [ -z ${HADOOP_HOME} ]; then
		echo "ERROR: Property HADOOP_HOME was not specified. You should export this in your ~/.bashrc." >&2
		_e=1
	fi
	if [ -z ${SPARK_HOME} ]; then
		echo "ERROR: Property SPARK_HOME was not specified. You should export this in your ~/.bashrc." >&2
		_e=1
	fi
	if [ -z ${HADOOP_USER_NAME} ]; then
		echo "ERROR: Property HADOOP_USER_NAME was not specified. You should export this in your ~/.bashrc." >&2
		_e=1
	elif [ ${HADOOP_USER_NAME} == "hadoop" ]; then
		echo "WARN: User 'hadoop' is discouraged. You should specify your personal user name."
	fi
	if [ ${_e} -ne 0 ]; then
		exit ${_e}
	fi

}


# read properties from file
function readConf() {

	if [ -f "${CONF_FILE}" ]; then
		. "${CONF_FILE}"
		_e=${?}
		if [ ${_e} -ne 0 ]; then
			echo "ERROR: Could not load ${CONF_FILE} (code ${_e})" >&2
			exit ${_e}
		fi
		checkProperties # check properties and environment variables
	else
		echo "ERROR: File ${CONF_FILE} does not exist" >&2
		exit 1
	fi

}


function checkMainJar() {

	# if main jar not provided, copy core jar from maven
	local _copyMainJar=false
	if [ -z ${MAIN_JAR} ]; then
		echo -e "\nINFO: Obtaining main jar from maven because property MAIN_JAR is not set (replacing jar in lib folder)..."
		_copyMainJar=true
	elif [ ! -f ${MAIN_JAR} ]; then
		echo -e "\nINFO: Obtaining main jar from maven because ${MAIN_JAR} is not found (replacing jar in lib folder)..."
		_copyMainJar=true
	fi
	if [ "${_copyMainJar}" = "true" ]; then
		mvn org.apache.maven.plugins:maven-dependency-plugin:2.8:copy \
				-DoutputDirectory="lib" \
				-Dmdep.overWriteSnapshots="true" \
				-Dartifact="${MAIN_ARTIFACT}"
		MAIN_JAR="lib/streams-spark-0.0.2-SNAPSHOT.jar"
	fi

	if [ -f ${MAIN_JAR} ]; then
		echo -e "INFO: ${MAIN_JAR} is the main jar archive\n"
	else
		echo -e "ERROR: Could not find file ${MAIN_JAR}\n"
		exit 1
	fi

}


# check if the argument is a file on HDFS
function isFileOnHdfs() {

    if (${HADOOP_HOME}/bin/hadoop fs -test -f $1) &> /dev/null ; then
        return 0
    else
        return 1
    fi

}


# Uploads the local file to HDFS
function uploadLocalFile() {

	local hdfsDir=$(echo "${XML_PATH}" | rev | cut -d'/' --complement -f1 | rev)

	# Try to put file on HDFS
	echo "INFO: Uploading $LOCAL_FILE to $XML_PATH on HDFS..."
	if [[ -f $LOCAL_FILE ]]; then 	# only proceed if $LOCAL_FILE is actually local

		${HADOOP_HOME}/bin/hadoop fs -mkdir -p $hdfsDir &> /dev/null	# make sure directory exists
		if [[ "$?" != "0" ]]; then
			echo -e "ERROR: Upload failed (VPN connected?)\n"
			exit 1
		fi
		${HADOOP_HOME}/bin/hadoop fs -rm $XML_PATH &> /dev/null		# remove target if present
		
		# hadoop fs -put with success check
		${HADOOP_HOME}/bin/hadoop fs -put $LOCAL_FILE $XML_PATH &> /dev/null
		if [[ "$?" = "0" ]]; then
			return 0 # everything okay
		else
			echo -e "ERROR: Upload failed\n" >&2
			exit 1
		fi

	else
		echo "ERROR: Could not find $LOCAL_FILE on local file system" >&2
		exit 1
	fi

}


# Shows a summary of what will be executed.
function showSummary() {

	kind="BATCH"
	if [[ "${STREAM}" = "true" ]]; then
		kind="STREAMING"
	fi

	cat <<- _EOF_

		----  ${kind} job  ---------------------------------------------
		configuration     ${XML_PATH}
		modules           ${MAIN_JAR} (main module)
	_EOF_
	for module in $(echo ${DEPENDENCY_JARS} | rev | sed "s/,/ /g"); do
		echo "                  $(echo ${module} | rev)"
	done

	cat <<- _EOF_
		--job-name        ${JOB_NAME}
	_EOF_
	if [[ "${STREAM}" = "true" ]]; then
		echo "--maxemptyrdds    ${MAXEMPTYRDDS}"
		echo "--numreceivers    ${NUMRECEIVERS}"
		echo "--block-interval  ${BLOCK_INTERVAL}"
	fi
	cat <<- _EOF_
		--driver-cores    ${DRIVER_CORES}
		--driver-memory   ${DRIVER_MEMORY}
		--maxresultsize   ${MAXRESULTSIZE}
		--num-executors   ${NUM_EXECUTORS}
		--executor-cores  ${EXECUTOR_CORES}
		--executor-memory ${EXECUTOR_MEMORY}
		----------------------------------------------------------------

	_EOF_

}


# Run on cluster
function runSparkJob() {

	echo -e "INFO: Submitting Spark Job...\n"

	${SPARK_HOME}/bin/spark-submit \
			--master yarn \
			--deploy-mode cluster \
			--class streams.spark.run \
			--name ${JOB_NAME} \
			--driver-cores ${DRIVER_CORES} \
			--driver-memory ${DRIVER_MEMORY} \
			--num-executors ${NUM_EXECUTORS} \
			--executor-cores ${EXECUTOR_CORES} \
			--executor-memory ${EXECUTOR_MEMORY} \
			--files "conf/log4j.properties,hdfs:///streams-spark/babar-agent-0.2.0-SNAPSHOT.jar" \
			--conf spark.yarn.jars=${DEPENDENCY_JARS},${MAIN_JAR} \
			--conf spark.yarn.submit.waitAppCompletion=${WAIT_APP_COMPLETION} \
			--conf spark.eventLog.enabled=true \
			--conf spark.eventLog.dir=${EVENTLOG_DIR} \
			--conf spark.streaming.blockInterval=${BLOCK_INTERVAL} \
			--conf spark.driver.maxResultSize=${MAXRESULTSIZE} \
			--conf spark.executor.extraJavaOptions="-javaagent:./babar-agent-0.2.0-SNAPSHOT.jar=StackTraceProfiler,JVMProfiler[reservedMB=2560],ProcFSProfiler" \
			${MAIN_JAR} \
			"--stream=${STREAM}" \
			"--maxemptyrdds=${MAXEMPTYRDDS}" \
			"--numreceivers=${NUMRECEIVERS}" \
			"${HDFS_BASE}${XML_PATH}"

	# check for errors
	if [ ${?} -ne 0 ]; then
		echo -e "\nERROR: Spark job failed (see logs for details)!\n" >&2
		exit 1
	fi

	# depending on --nowait, the exit status of spark-submit indicates different things
	if [[ "${WAIT_APP_COMPLETION}" == "false" ]] ; then
		echo -e "\nINFO: Spark job accepted\n"
	else
		echo -e "\nINFO: Spark job executed successfully\n"
	fi

}
