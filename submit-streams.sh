#!/bin/bash
# 
# streams-spark - an Apache Spark extension for the streams framework
# 
# Copyright (C) 2016 by the participants of the Project Group 594 at
# the University of Technology in Dortmund: Mohamed Asmi, Alexander
# Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
# Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
# Carolin Wiethoff and Lili Xu.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/.
# 
_e=0
_prg="$( basename ${0} )"
_timestamp="$(TZ="Europe/Berlin" date +"%y%m%d-%H%M%S")"


# runtime argument defaults
JOB_NAME="streams-spark"
STREAM="false"
MAXEMPTYRDDS="60"
NUMRECEIVERS="-1"
BLOCK_INTERVAL="250ms"
DRIVER_CORES="2"
DRIVER_MEMORY="8g"
MAXRESULTSIZE="${DRIVER_MEMORY}"
NUM_EXECUTORS="5"
EXECUTOR_CORES="2"
EXECUTOR_MEMORY="4g"
WAIT_APP_COMPLETION="false"
MAIN_JAR=
XML_PATH=
LOCAL_FILE=


# read bashlib/submit-streams.lib.sh
SUBMIT_STREAMS_LIB="bashlib/submit-streams.lib.sh"
if [ -f $SUBMIT_STREAMS_LIB ]; then
	. $SUBMIT_STREAMS_LIB
	_e=${?}
	if [ ${_e} -ne 0 ]; then
		echo "ERROR: Could not load ${SUBMIT_STREAMS_LIB} (code ${_e})" >&2
		exit ${_e}
	fi
else
	echo "ERROR: File ${SUBMIT_STREAMS_LIB} does not exist" >&2
	exit 1
fi


# exit if no argument is provided
if [[ $# = 0 || $1 = "--help" ]]; then
    showManpage
    exit 2
fi


# YARN configuration
export YARN_CONF_DIR=$HADOOP_CONF_DIR
echo -e "\nINFO: Using $YARN_CONF_DIR as YARN_CONF_DIR and HADOOP_CONF_DIR"

# obtain HDFS base URL
HDFS_BASE=$(getDefaultFS)
echo "INFO: The HDFS is located at $HDFS_BASE"


readConf # read conf/submit-streams.conf.sh


# eval runtime arguments
while [[ $# > 0 ]]; do
	case $1 in
		--job-name)
			JOB_NAME="${2}"
			shift
			;;
		--stream)
			STREAM="true"
			;;
		--streaming)
			STREAM="true"
			echo "WARN: The flag 'streaming' is discouraged. For convenience, it is interpreted as 'stream' for now."
			;;
		--maxemptyrdds)
			MAXEMPTYRDDS="${2}"
			shift
			;;
        --numreceivers)
            NUMRECEIVERS="${2}"
            shift
            ;;
		--block-interval)
			BLOCK_INTERVAL="${2}"
			shift
			;;
		--driver-cores)
			DRIVER_CORES="${2}"
			shift
			;;
		--driver-memory)
			DRIVER_MEMORY="${2}"
			shift
			;;
		--maxresultsize)
			MAXRESULTSIZE="${2}"
			shift
			;;
		--num-executors)
			NUM_EXECUTORS="${2}"
			shift
			;;
		--executor-cores)
			EXECUTOR_CORES="${2}"
			shift
			;;
		--executor-memory)
			EXECUTOR_MEMORY="${2}"
			shift
			;;
		--wait)
			WAIT_APP_COMPLETION="true"
			;;
		--local)
			LOCAL_FILE="${2}"
			shift
			;;
		--help)
			showManpage
			exit 0
			;;
		--)
			;;
		* ) # <xml file> argument: only allow one of such
			if [ -z ${XML_PATH} ]; then
				XML_PATH="${1}"
			else
				echo -e "ERROR: Unknown argument" >&2
				exit 2
			fi
			;;
	esac
	shift
done
if [ -z ${XML_PATH} ]; then
	showManpage
	exit 2
fi


# prepare execution
checkMainJar
echo "INFO: Make sure you are connected to the VPN (or starting from a cluster node)"


# upload if option --local is provided
if [ ! -z ${LOCAL_FILE} ]; then
	uploadLocalFile

# if --local is not provided, check if file is present on HDFS
elif ! isFileOnHdfs ${XML_PATH}; then
	echo -e "ERROR: File ${XML_PATH} not present on HDFS (VPN connected?)\n"
	exit 1
fi


showSummary # show summary of what will be executed
runSparkJob # run job on cluster

