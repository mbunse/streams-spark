Apache Spark Extension for the streams Framework
========


This extension enables [the streams framework](https://sfb876.de/streams/)
(also see [streams @ Bitbucket](https://bitbucket.org/cbockermann/streams))
to run on an [Apache Spark](http://spark.apache.org/)/YARN cluster. You can submit
your streams' XML configurations just like you are used to, leveraging the full
power of your cluster.

The first version of this extension was developed by the Project Group 594 at the University of Technology in Dortmund.
It was supported by the Deutsche Forschungsgemeinschaft (DFG) within the Collaborative Research Center SFB 876
"Providing Information by Resource-Constrained Analysis", project C3.
Feel free to contribute!



## Preliminaries
Hadoop 2.7 and Spark 2.1.0 should be installed on your local machine to make the scripts work.
You can obtain them from the respective project websites [spark.apache.org](http://spark.apache.org/downloads.html)
and [hadoop.apache.org](http://hadoop.apache.org/releases.html).
Moreover, a VPN into your Spark/YARN cluster should be set up and running.

Set the following environment variables according to your installation (e.g., using `export` in your `~/.bashrc`):

* HADOOP_HOME (e.g., '/usr/local/hadoop-2.7.0/')
* SPARK_HOME (e.g., '/usr/local/spark-2.1.0/')
* HADOOP_USER_NAME (e.g., 'bunse'; 'hadoop' is not encouraged)

Make yourself familiar with the [HDFS shell](https://hadoop.apache.org/docs/r2.7.0/hadoop-project-dist/hadoop-common/FileSystemShell.html).
This will help you manage the files stored in the Hadoop Distributed File System
(most commands are similar to regular shell commands).



## Usage
You can build the project with Maven:

      $ mvn package

To run a streams job in a Spark cluster, you should use the `submit-streams.sh` script from
the streams-spark-submit module. Have a look at its man page to see how it is used:

      $ cd streams-spark-submit/
      $ ./streams-submit.sh --help

Having started a job with `submit-streams.sh`, use the HDFS and YARN web frontends to monitor
your jobs and file system contents.



## Dependency Management
Running streams on Spark requires a set of dependencies with a total size of more than 100MB.
You should have their jars placed in the `/jars` folder on HDFS to make the scripts work.
If not yet there, you can find the jars in the `jars` folder of you local Spark installation
and upload them using the HDFS shell.



## More Documentation
A JavaDoc can be created with maven and viewed by opening the generated `target/site/apidocs/index.html`:

      $ mvn javadoc:javadoc

Moreover, there is a TechReport at [the SFB 876 homepage](http://sfb876.tu-dortmund.de/auto?self=$Publication_ew8bpgikg0)
which documents the streams-spark extension and other results of the PG 594 (some details may be outdated).



## Contact
Feel free to contact any of the current maintainers:
[Mirko Bunse](https://bitbucket.org/mbunse/),
[Michael May](https://bitbucket.org/maymic/),
and [Karl Stelzner](https://bitbucket.org/KarlSt/).
You may also want to have a look at [the issues page](https://bitbucket.org/mbunse/streams-spark/issues).

Contributors to the first version are the participants of the PG 594, namely Mohamed Asmi, Alexander Bainczyk, Mirko Bunse,
Dennis Gaidel, Michael May, Christian Pfeiffer, Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
Carolin Wiethoff, and Lili Xu.



## Related Extensions
Other parts of the PG 594 results include a restful API for the cluster with a graphical web interface (not available yet)
and a Spark extension for the [FACT](http://sfb876.tu-dortmund.de/FACT/)-tools, named
[fact-tools-spark](https://bitbucket.org/mbunse/fact-tools-spark).


