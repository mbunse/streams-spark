/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package streams.spark;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.runtime.ElementHandler;

/**
 * Run class for the streams-spark extension.
 * 
 * It will initialize the {@link StreamsSparkContext} and interpret the
 * extension's Streams elements.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 */
public class run {

    private static Logger log = LoggerFactory.getLogger(run.class);

	/**
	 * Main method
	 * 
	 * @param args
	 *            Runtime arguments as handled by
	 *            {@link runContext#runContext(String[])}.
	 * @throws Exception
	 *             May throw any Exception.
	 * 
	 * @see stream.run#handleArguments(String[])
	 */
    public static void main(String[] args) throws Exception {
    	
    	log.info("Using the CORE run class implementation");

    	runContext rc = new runContext(args);	// will interpret arguments
    	Map<String, ElementHandler> customElementHandlers = 
    			rc.getCoreElementHandlers();
    	
		/*
		 * Any run class that overrides this one may easily fill the
		 * customElementHandlers map with more handlers.
		 */
    	
    	rc.execute(customElementHandlers);

    }

}
