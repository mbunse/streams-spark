/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package streams.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.w3c.dom.Document;

/**
 * Wraps Spark's {@link JavaSparkContext} and {@link JavaStreamingContext}
 * together with broadcasted variables to be accessible from both driver and
 * executors.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 */
public class StreamsSparkContext {

    /**
     * Mini-batch size used in a Streaming Context, given in ms.
     */
    public final static int STREAMING_INTERVAL = 500;

    /**
     * Time to remember data items in a Streaming Context, given in ms.
     */
    public final static int STREAMING_REMEMBER = 10000;

    private static StreamsSparkContext INSTANCE;

    /**
	 * Initializes the StreamsSparkContext singleton.
	 *
	 * @param xmlDoc
	 *            The document object of the XML configuration file
	 * @param stream
	 *            True, iff this should be the context of a Spark Streaming job.
	 *            Otherwise, it is the context of a batch job.
	 * @param maxEmptyRdds
	 *            The maximum number of empty RDDs that a Streaming job sees in
	 *            a row before terminating. Does not apply to batch jobs.
	 *            
	 * @return The initialized singleton instance, which can also be accessed by
	 *         {@link #getInstance()}.
	 *         
	 * @throws IllegalStateException
	 *             When this method was called before.
	 */
	public static StreamsSparkContext init(Document xmlDoc, boolean stream,
			int maxEmptyRdds, int numReceivers) {

        // make sure, instance is only created once!
        if (StreamsSparkContext.INSTANCE == null) {

			StreamsSparkContext.INSTANCE = new StreamsSparkContext(xmlDoc,
					stream, maxEmptyRdds, numReceivers);
			return StreamsSparkContext.INSTANCE;

        } else {
            throw new IllegalStateException("StreamsSparkContext "
                    + "is already initialized!");
        }

    }

	/**
	 * @return Singleton instance of this class.
	 * @throws IllegalStateException
	 *             When {@link #init(Document, boolean, int, int)} was not
	 *             called before.
	 */
    public static StreamsSparkContext getInstance() throws IllegalStateException {

        // make sure, instance exists
        if (StreamsSparkContext.INSTANCE != null) {
            return StreamsSparkContext.INSTANCE;
        } else {
            throw new IllegalStateException("StreamsSparkContext "
                    + "has to be initialized before being requested!");
        }

    }
    
	/**
	 * Shorthand for {@link StreamsSparkContext}.{@link #getInstance()}.
	 * {@link #getJavaContext()}.
	 * 
	 * @return The {@link JavaSparkContext} of this execution
	 */
    public static JavaSparkContext sc() {
    	return StreamsSparkContext.getInstance().getJavaContext();
    }
    
	/**
	 * Shorthand for {@link StreamsSparkContext}.{@link #getInstance()}.
	 * {@link #getStreamingContext()}.
	 * 
	 * @return The {@link JavaStreamingContext} of this execution
	 */
    public static JavaStreamingContext ssc() {
    	return StreamsSparkContext.getInstance().getStreamingContext();
    }

    private transient final JavaSparkContext context;
    private transient final JavaStreamingContext streamingContext;
    private final Document xmlDoc;
    private final boolean stream;
    private final int maxEmptyRdds;
    private final int numReceivers;

	/**
	 * @param xmlDoc
	 *            The XML process configuration for the streams framework
	 * @param stream
	 *            True, iff this should be the context of a Spark Streaming job.
	 *            Otherwise, it is the context of a batch job.
	 * @param maxEmptyRdds
	 *            The maximum number of empty RDDs that a Streaming job sees in
	 *            a row before terminating. Does not apply to batch jobs.
	 */
    private StreamsSparkContext(Document xmlDoc, boolean stream, int maxEmptyRdds, int numReceivers) {

        SparkConf sparkConf = new SparkConf();
        this.context = new JavaSparkContext(sparkConf);
        this.xmlDoc = xmlDoc;
        this.stream = stream;
        this.maxEmptyRdds = maxEmptyRdds;
        this.numReceivers = numReceivers;

        this.streamingContext = new JavaStreamingContext(this.context, Durations.milliseconds(STREAMING_INTERVAL));
        this.streamingContext.remember(Durations.milliseconds(STREAMING_REMEMBER));

    }

	/**
	 * @return The Document object of the XML configuration file. Note that
	 *         using broadcasting, this method can be called from worker nodes,
	 *         too.
	 */
    public Document getXmlDoc() {
        return this.xmlDoc;
    }

	/**
	 * @return True, iff this is a Spark Streaming job. Otherwise, it is a batch
	 *         job.
	 */
	public boolean isStreamingJob() {
		return stream;
	}

	/**
	 * @return The maximum number of empty RDDs that a Streaming job sees in a
	 *         row before terminating. Does not apply to batch jobs.
	 */
	public int getMaxEmptyRdds() {
		return maxEmptyRdds;
	}

	/**
	 *
	 * @return The number of receivers to start.
	 */
	public int getNumReceivers() {
		return numReceivers;
	}

	/**
     * @return Spark's {@link JavaSparkContext} for this application
     */
    public JavaSparkContext getJavaContext() {
        return this.context;
    }

    /**
     * @return Spark's {@link JavaStreamingContext} for this application
     */
    public JavaStreamingContext getStreamingContext() {
        return streamingContext;
    }

	/**
	 * Wraps {@link JavaSparkContext#broadcast(Object)} for the JavaSparkContext
	 * of this object.
	 *
	 * @param <T>
	 *            Arbitrary type of this broadcast
	 * @param value
	 *            The value to broadcast
	 * @return A handle to receive the broadcast object at a worker
	 * 
	 * @see #getJavaContext()
	 * @see <a href="http://spark.apache.org/docs/latest/programming-guide.html#broadcast-variables">
	 *      The Spark Programming Guide on http://spark.apache.org/</a>
	 */
    public <T> Broadcast<T> broadcast(T value) {
        return this.context.broadcast(value);
    }
    
}
