/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package streams.spark;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import stream.io.SourceURL;
import stream.runtime.ElementHandler;
import stream.runtime.ProcessContainer;
import stream.runtime.StreamRuntime;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.factory.ProcessorFactory;
import stream.runtime.setup.handler.DProcessElementHandler;
import stream.util.Variables;

/**
 * Utilities for the run class (so that it can be overridden with the least
 * possible redundancy).
 * 
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 */
public class runContext {
	
    private static Logger log = LoggerFactory.getLogger(runContext.class);
    
    private Variables vars;
    private Document xmlDoc;
    private boolean streaming;
    private int maxEmptyRdds;
    private int numReceivers;
    private ObjectFactory objectFactory;
    private ProcessorFactory processorFactory;
    
    private ProcessContainer container;

	/**
	 * Constructor of this class
	 * 
	 * @param args
	 *            Runtime arguments as handled by
	 *            {@link stream.run#handleArguments(String[])}.
	 * @throws Exception
	 *             May throw any Exception.
	 */
    public runContext(String[] args) throws Exception {
    	
    	// default initialization
        List<String> params = stream.run.handleArguments(args);
        if (params == null || params.isEmpty()) {
            return;
        }
        
        stream.run.setupOutput();
		StreamRuntime.setupLogging();
        String streamsVersion = stream.run.getVersion();
        if (streamsVersion != null)
        	log.info("Will run on the {} version of the Streams framework", streamsVersion);
        
		// streams-spark properties
		this.streaming = System.getProperty("stream", "false").equals("true");
        this.maxEmptyRdds = Integer.parseInt(System.getProperty("maxemptyrdds", "120"));
        this.numReceivers = Integer.parseInt(System.getProperty("numreceivers", "-1"));
        log.info("Running a {} job", this.streaming ? "STREAMING" : "BATCH");
		
		this.vars = StreamRuntime.loadUserProperties();
        log.info("Setting up streams-spark for configuration at {}", params.get(0));

        // XML parsing and context initialization
        this.xmlDoc = parseHdfsDocument(params.get(0));
        StreamsSparkContext.init(this.xmlDoc, this.streaming, this.maxEmptyRdds, this.numReceivers);

        // element handler registration
        this.objectFactory = ObjectFactory.newInstance();
        this.processorFactory = new ProcessorFactory(objectFactory);

    }
    
	/**
	 * @return A mapping for element handlers required by the core module
	 */
    public Map<String, ElementHandler> getCoreElementHandlers() {
    	
    	Map<String, ElementHandler> coreElementHandlers = new HashMap<>();
    	
		coreElementHandlers.put(DProcessElementHandler.ELEMENT_TAG,
				new DProcessElementHandler(this.objectFactory, this.processorFactory, false));
		
		return coreElementHandlers;
		
    }

	/**
	 * Initialize and execute a ProcessContainer. It will use the provided
	 * element handlers, the XML document and {@link Variables} object created
	 * in the constructor.
	 *
	 * @param customElementHandlers
	 *            A mapping from element tags to element handlers
	 * @throws Exception
	 *             Any exception that may occur during execution
	 */
    public void execute(Map<String, ElementHandler> customElementHandlers) throws Exception {
    	
        log.trace("Starting process container...");
        this.container = new ProcessContainer(this.xmlDoc, customElementHandlers, this.vars);
        this.container.execute();
        
        log.trace("Container finished.");
        
    }
    

    /**
     * Copied from {@link ProcessContainer#parseDocument(URL)} to accept
     * a String URL.
     */
    private static Document parseHdfsDocument(String url) throws Exception {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(new SourceURL(url).openStream()); // only difference

    }

}
