package streams.spark.example;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.Sink;

/**
 * Sink that writes the processing time of each item into a comma separated file.
 * 
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * @author Mohamed Asmi &lt;mohamed.asmi@tu-dortmund.de&gt;
 */
public class PerformanceSink implements Sink {
	
	private transient final static Logger log = LoggerFactory
			.getLogger(PerformanceSink.class);

	private String id;
	private String url;
	
	private transient FileSystem hdfs;
	private transient BufferedWriter bw;
	private transient long startTime;
	private transient long numItems;
	
	public PerformanceSink() {
		// Nothing to do
	}
	
	public PerformanceSink(String url) throws Exception {
		this.setUrl(url);
	}

	@Override
	public void init() throws Exception {
		
		// check initialization
		if (this.url == null)
			log.warn("No URL was set for sink {}. Will not write performance to any file.");
		
		this.startTime = System.currentTimeMillis();	// set start time
		this.numItems = 0;
		
	}

	@Override
	public boolean write(Data data) throws Exception {
		
		if (this.hdfs != null) {
		
			// perform writer initialization on first write
			if (this.bw == null)
				this.bw = new BufferedWriter(new OutputStreamWriter(
						this.hdfs.create(new Path(this.url), true)));
			
			// write processing time to file
			try {
				this.bw.write(Long.toString(System.currentTimeMillis() - this.startTime));
				this.bw.newLine();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			
		}
		
		this.numItems++;
		return true;
		
	}

	@Override
	public boolean write(Collection<Data> collection) throws Exception {
		for(Data data : collection) {
			boolean success = this.write(data);
			if (!success)
				return false;
		}
		return true;
	}
	
	@Override
	public void close() throws Exception {
		if (this.bw != null) {
			this.bw.flush();
			this.bw.close();
		}
		log.info("Received {} items with an approximate rate of {} items per second",
				this.numItems, String.format("%.3f",
						1000.0 * this.numItems / (System.currentTimeMillis() - this.startTime)));
	}

	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return The path of the result file
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            The path of the result file
	 * @throws Exception
	 *             Any exception thrown by initializing a Hadoop
	 *             {@link FileSystem} object.
	 */
	public void setUrl(String url) throws Exception {
		
		// find HDFS
		Matcher fsMatcher = Pattern.compile("^[a-z]+://[^:]+:[0-9]+").matcher(url);
		if (!fsMatcher.find())
			throw new IllegalArgumentException(
					"Invalid URL format, filesystem could not be determined: " + url);
		FileSystem hdfs = FileSystem.get(new URI(fsMatcher.group()), new Configuration());
		
		// set URL and buffered writer
		this.url = url;
		this.hdfs = hdfs;
		
	}

}
