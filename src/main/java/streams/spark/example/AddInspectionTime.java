/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package streams.spark.example;

import stream.Data;
import stream.Processor;

/**
 * Simple processor adding a time stamp of when the item was inspected by this
 * processor.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 *
 */
public class AddInspectionTime implements Processor {

	/** Default key to put the inspection time **/
	public final static String DEFAULT_KEY = "inspectionTime";
	
	private String key = DEFAULT_KEY;

	@Override
	public Data process(Data input) {
		input.put(this.key, System.currentTimeMillis());
		return input;
	}

	/**
	 * @return The key where the time stamps are stored in each {@link Data}
	 *         item.
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            The key where the time stamps are stored in each {@link Data}
	 *            item. By default, this is "inspectionTime".
	 */
	public void setKey(String key) {
		this.key = key;
	}

}
