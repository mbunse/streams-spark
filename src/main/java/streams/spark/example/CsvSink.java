package streams.spark.example;

import java.net.URI;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.CsvWriter;
import stream.io.Sink;

/**
 * Sink which writing the given dataitems into a .csv file on HDFS, using the
 * {@link CsvWriter}.
 * 
 * @author Karl Stelzner &lt;karl.stelzner@uni-dortmund.de&gt;
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 */
public class CsvSink implements Sink {
	
	private transient final static Logger log = LoggerFactory
			.getLogger(CsvSink.class);
	
	private String id;
	private String url;
	
	private transient FileSystem hdfs;
	private transient CsvWriter writer = null;

	@Override
	public void init() throws Exception {
		// nothing to do
	}
	
	@Override
	public boolean write(Data data) throws Exception {
		
		if (this.hdfs != null) {
		
			// initialize writer on first write
			if (this.writer == null) {
				this.writer = new CsvWriter(hdfs.create(new Path(url), true));
				this.writer.init(null);
				this.writer.writeHeader(data);
			}
			
			this.writer.write(data);
			
		}
		
		return true;
		
	}

	@Override
	public boolean write(Collection<Data> collection) throws Exception {
		for(Data data : collection)
			this.write(data);
		return true;
	}

	@Override
	public void close() throws Exception {
		if (writer != null)
			writer.finish();
		else
			log.warn("Closing CsvSink " + this.getId() + " of which the URL was never set.");
	}
	
	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return The path of the result file
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * @param url
	 *            The path of the result file
	 * @throws Exception
	 *             Any exception thrown by initializing a Hadoop
	 *             {@link FileSystem} object.
	 */
	public void setUrl(String url) throws Exception {
		
		// find HDFS
		Matcher fsMatcher = Pattern.compile("^[a-z]+://[^:]+:[0-9]+").matcher(url);
		if (!fsMatcher.find())
			throw new IllegalArgumentException(
					"Invalid URL format, filesystem could not be determined: " + url);
		FileSystem hdfs = FileSystem.get(new URI(fsMatcher.group()), new Configuration());
		
		// set URL and buffered writer
		this.url = url;
		this.hdfs = hdfs;
		
	}
	
}
