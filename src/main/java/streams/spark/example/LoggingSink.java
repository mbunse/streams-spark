/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package streams.spark.example;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.Sink;

/**
 * Simple sink logging {@link Data} items to the console.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 *
 */
public class LoggingSink implements Sink {
	
	private String id;
	private Logger log;

	private boolean onlyLogNumItems = false;
	private int totalNumItems = 0;

	@Override
	public void init() throws Exception {
		this.log = LoggerFactory.getLogger(
				LoggingSink.class.getCanonicalName() + "-" + this.getId());
	}

	@Override
	public boolean write(Data item) throws Exception {
		if (log != null) {
			
			this.totalNumItems++;
			log.info(this.onlyLogNumItems
					? "Received 1 item (" + this.totalNumItems + " total)."
					: item.toString());
			return true;
			
		} else {
			throw new IllegalStateException("Sink not initialized (closed?)");
		}
	}

	@Override
	public boolean write(Collection<Data> items) throws Exception {
		if (this.onlyLogNumItems) {
			
			this.totalNumItems += items.size();
			log.info("Received {} items ({} total).", items.size(),
					this.totalNumItems);
			
		} else {
			
			for (Data item : items)
				this.write(item);
			
		}
		return true;
	}
	
	@Override
	public void close() throws Exception {
		log.info("This sink is now closed.");
		log = null;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	/**
	 *
	 * @return True, iff only the number of received items should be printed,
	 *         not their content.
	 */
	public boolean isOnlyLogNumItems() {
		return this.onlyLogNumItems;
	}

	/**
	 *
	 * @param onlyLogNumItems
	 *            True, iff only the number of received items should be printed,
	 *            not their content.
	 */
	public void setOnlyLogNumItems(boolean onlyLogNumItems) {
		this.onlyLogNumItems = onlyLogNumItems;
	}

}
