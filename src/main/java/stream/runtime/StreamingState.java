/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import java.io.Serializable;

import streams.spark.StreamsSparkContext;

/**
 * Some state used by the {@link StreamingMonitor}.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 */
public class StreamingState implements Serializable {

    private static final long serialVersionUID = -1079864633252562383L;

    /**
     * {@link org.apache.spark.AccumulatorParam} enabling the
     * {@link StreamingState} to be used in an accumulator.
     * 
     * Note: Using an accumulator for the state is not mandatory!
     *
     * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
     */
    @Deprecated
    public static class AccumulatorParam implements
            org.apache.spark.AccumulatorParam<StreamingState> {

        private static final long serialVersionUID = 3888306809219734086L;

        @Override
        public StreamingState zero(StreamingState arg0) {
            return arg0;
        }

        @Override
        public StreamingState addInPlace(StreamingState arg0,
                                                  StreamingState arg1) {
            arg0.numEmptyRdds = Math.max(arg0.numEmptyRdds, arg1.numEmptyRdds);
            arg0.isStarted = arg0.isStarted || arg1.isStarted;
            arg0.isStopped = arg0.isStopped || arg1.isStopped;
            arg0.hasFailed = arg0.hasFailed || arg1.hasFailed;
            return arg0;
        }

        @Override
        public StreamingState addAccumulator(StreamingState arg0,
                                                      StreamingState arg1) {
            return this.addInPlace(arg0, arg1);
        }

    } // end of AccumulatorParam

    private final String processId;
    private final int maxEmptyRdds;
    private boolean isStarted = false;
    private boolean isStopped = false;
    private boolean hasFailed = false;
    private int numEmptyRdds = 0;

    /**
     * @param context The process this finishedReceivers is associated to
     */
    public StreamingState(DProcessContext context) {
        this.processId = context.getProcessId();
        this.maxEmptyRdds = StreamsSparkContext.getInstance().getMaxEmptyRdds();
    }

    /**
     * Marks this process as started.
     *
     * @throws IllegalStateException Thrown, if the process is marked as started already
     */
    public synchronized void markStarted() throws IllegalStateException {
        if (!this.isStarted)
            this.isStarted = true;
        else
            throw new IllegalStateException("State of process "
                    + this.processId + " is already marked as started!");
    }

    /**
     * Marks this process as stopped.
     *
     * @param hasFailed Should be set to true iff this process should be stopped due
     *                  to failure.
     * @throws IllegalStateException Thrown, if the process is marked as stopped already, or if it
     *                               was not marked as started before
     */
    public synchronized void markStopped(boolean hasFailed) throws IllegalStateException {
        if (!this.isStopped && this.isStarted) {
            this.isStopped = true;
            this.hasFailed = hasFailed;
        } else if (this.isStopped)
            throw new IllegalStateException("State of process "
                    + this.processId + " is already marked as stopped!");
        else
            throw new IllegalStateException("State of process "
                    + this.processId + " has to be marked as started first!");
    }

    /**
     * Increments the number of empty RDDs seen subsequently.
     */
    public synchronized void increaseNumEmptyRdds() {
        this.numEmptyRdds++;
    }

    /**
     * Sets the number of empty RDDs seen subsequently to zero.
     */
    public synchronized void zeroEmptyRdds() {
        this.numEmptyRdds = 0;
    }

    /**
     * @return True iff this process is marked as started
     */
    public boolean isStarted() {
        return isStarted;
    }

    /**
     * @return True iff this process is marked as stopped
     */
    public boolean isStopped() {
        return isStopped;
    }

    /**
     * @return True iff this process was stopped due to failure.
     */
    public boolean hasFailed() {
        return hasFailed;
    }

    /**
     * @return Number of empty RDDs seen subsequently
     */
    public int getNumEmptyRdds() {
        return numEmptyRdds;
    }

	/**
	 * @return The maximum number of empty RDDs seen subsequently before
	 *         termination
	 */
	public int getMaxEmptyRdds() {
		return maxEmptyRdds;
	}

	@Override
    public String toString() {
        return "[isStarted=" + isStarted + ", isStopped=" + isStopped
                + ", numEmptyRdds=" + numEmptyRdds + "/" + maxEmptyRdds + "]";
    }

}
