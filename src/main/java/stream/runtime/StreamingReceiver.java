/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Process;
import stream.io.ReceiverSink;
import stream.io.Stream;
import stream.io.multi.LazySeqMultiStream;
import stream.io.multi.MultiStream;
import stream.runtime.ElementHandler;
import stream.runtime.ProcessContainer;
import stream.runtime.StreamRuntime;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.factory.ProcessorFactory;
import stream.runtime.setup.handler.DProcessElementHandler;
import stream.util.Variables;

/**
 * Receiver that reads from a set of streams, processes them and stores a
 * continuous stream of processed items in Spark Streaming.
 *
 * @author Karl Stelzner &lt;karl.stelzner@tu-dortmund.de&gt;
 * 
 */
public class StreamingReceiver extends Receiver<Data> {

    private transient static Logger log = LoggerFactory.getLogger(StreamingReceiver.class);
    private final static long serialVersionUID = -3536119501663636816L;

    private DProcessContext context;
    private List<String> substreamIds;

    /**
     * Creates a receiver for the given process context.
     *
     * @param context The process context
     */
    public StreamingReceiver(DProcessContext context) {
        this(context, null);
    }

    /**
     * Creates a receiver for the given process context. If the process has a
     * {@link MultiStream} as input, a list of sub-stream IDs can be provided
     * to specify which sub-streams should be read by this receiver.
     *
     * @param context      The process context
     * @param substreamIds List of sub-stream IDs. If this is null, the complete process
     *                     input is read. Values unequal to null are only allowed for
     *                     {@link MultiStream} inputs!
     */
    public StreamingReceiver(DProcessContext context, List<String> substreamIds) {
        super(StorageLevel.MEMORY_AND_DISK());
        this.context = context;
        this.substreamIds = substreamIds;
    }

    @Override
    public void onStart() {

        // receive data in a separate thread
        new Thread() {
            @Override
            public void run() {
                StreamingReceiver.this.receive();
            }
        }.start();

    }

    @Override
    public void onStop() {
		/*
		 * nothing to do because receiving thread will take care of this event
		 * on its own
		 */
    }

	/**
	 * Starts processing and receiving {@link Data} items. Should only be called
	 * in a separate thread.
	 */
    private void receive() {
        try {
        	
            // prepare the Streams framework
            final Variables vars = StreamRuntime.loadUserProperties();

            // custom element handler registration
            final Map<String, ElementHandler> customElementHandlers = new HashMap<>();
            final ObjectFactory objectFactory = ObjectFactory.newInstance();
            final ProcessorFactory processorFactory = new ProcessorFactory(objectFactory);
            customElementHandlers.put(DProcessElementHandler.ELEMENT_TAG,
                    new DProcessElementHandler(objectFactory, processorFactory, true));

			// create the process container
			ProcessContainer container = new ProcessContainer(this.context.getXmlDoc(), customElementHandlers, vars);
			
			// find process in container and remove all others
			Process process = null;
			for (Process p : container.getProcesses()) {
				if (p.getProperties().get("id").equals(this.context.getProcessId())) {
					process = p;
					break;
				} 
			}
			if (process == null)
				this.stop("Could not find input of process with ID '"
						+ this.context.getProcessId() + "'!");
			container.getProcesses().clear();
			container.getProcesses().add(process);
			
			// if sub-streams are specified, then they must exist!
			if (this.substreamIds != null && !(process.getInput() instanceof MultiStream)) {
				log.error(
						"Process source with ID {} is no instance of MultiStream! "
						+ "Proceeding without limiting this source to substream IDs.",
						this.context.getProcessId());
				this.substreamIds = null;
			}
			
			// create sequential MultiStream from sub-stream IDs
			LazySeqMultiStream newInput = new LazySeqMultiStream() {
				@Override	// anonymous subclass adds the worker ID to each data item
				public Data readNext() throws Exception {
					Data item = super.readNext();
					if (item != null){
						item.put(StreamingReceiver.this.context.getWorkerIdKey(),
								StreamingReceiver.this.streamId());
                    }
					return item;
				}
			};

            // set specified sub-streams as input
            if (this.substreamIds != null) {
                for (String substreamId : this.substreamIds)
                    newInput.addStream(substreamId, ((MultiStream) process.getInput()).getStreams().get(substreamId));
            } else {
                newInput.addStream(process.getInput().getId(), (Stream) process.getInput());
            }
            process.setInput(newInput);
            container.streams.clear();
            container.streams.put("Receiver" + this.streamId(), newInput);
            log.info("Receiver {} STARTING to receive from {} substream(s).", this.streamId(), newInput.getStreams().size());

            // store output in DStream
            process.setOutput(new ReceiverSink(this.streamId(), this));
            container.execute();

            log.info("FINISHED receiver {}", this.streamId());

        } catch (Exception e) {
            this.stop("Error reading input data", e);
        }

    }
    
}
