/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import java.io.Serializable;

import org.apache.spark.api.java.JavaRDD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.LinkedListSink;
import stream.io.RddSink;
import stream.io.multi.MultiStream;
import streams.spark.StreamsSparkContext;

/**
 * Distributed batch process distributing the sub-streams of a
 * {@link MultiStream} to be processed in parallel. Distribution is done by a
 * {@link DProcessExecution}, either running in batch or streaming mode.
 * 
 * @see BatchExecution
 * @see StreamingExecution
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 */
public class DProcess extends AbstractProcess implements Serializable {
	
	/** Default key to store the worker ID in each {@link Data} item */
	public final static String DEFAULT_WORKER_ID_KEY = "streams-spark-workerId";

    private final static long serialVersionUID = 1666190685883273119L;
    private transient final static Logger log = LoggerFactory.getLogger(DProcess.class);
    
    private String workerIdKey = DProcess.DEFAULT_WORKER_ID_KEY;
    
    private DProcessContext context;
    private DProcessExecution executor;

    @Override
    public void execute() throws Exception {
    	
    	// check that distribution is reasonable
        if (this.getInput() instanceof MultiStream) {
        	
        	MultiStream input = (MultiStream) this.getInput();
        	
        	// warn if distribution will only happen to single node
        	if (input.getStreams().size() < 2)
        		log.warn("Process {} will run on a single executor because its input {} "
        				+ "contains less than 2 sub-streams",
        				this.getId(), input.getId());
        	
        	// initialize execution based on streaming/batch semantic
        	if (StreamsSparkContext.getInstance().isStreamingJob())
        		this.executor = new StreamingExecution();
        	else
        		this.executor = new BatchExecution();
        	
        	this.context = new DProcessContext(this);
        	this.executor.init(this.context, input, this.getOutput());

        } else {
        	
        	/*
        	 * TODO Also in this case, stick to the single sub-stream semantic from above
        	 */

        	// distribution not possible - execute as DefaultProcess later
        	log.warn("Process {} can not be distributed because its input {} "
                    + "is not a MultiStream!",
                    this.getId(), this.getInput().getId());

        }

        // make sure distribution to workers happened
        if (this.executor != null)
        	this.executor.execute();
        else
        	this.executeAsDefaultProcess();

    }
    
	/**
	 * Behave like a {@link DefaultProcess} in local execution, but create an
	 * RDD from results, if output is an {@link RddSink}.
	 * 
	 * @throws Exception
	 *             Any exception thrown during the process
	 * 
	 * @see DefaultProcess#execute()
	 */
    private void executeAsDefaultProcess() throws Exception {
    	
    	if (this.getOutput() != null && this.getOutput() instanceof RddSink) {
			
			RddSink rddSink = (RddSink) this.getOutput();
			LinkedListSink llSink = new LinkedListSink();

			// connect to linked list sink
			log.info("Process {} executing as default process...", this.getId());
			this.setOutput(llSink);
            super.execute();    // normal process writing to llSink
            
            log.info("Distributing results of {}...", this.getId());
			JavaRDD<Data> resultRdd = StreamsSparkContext.sc().parallelize(llSink);
            
			log.info("Process {} forwarding result RDD to sink {}...",
					this.getId(), rddSink.getId());
			rddSink.write(resultRdd);

		} else {

            log.info("Process {} executing as default process..");
            super.execute();    // if not, just behave like normal process
            
		}
    	
    }

    /**
     * @return The {@link Data} key to which worker nodes should write their IDs
     */
    public String getWorkerIdKey() {
    	
    	if (this.context != null)	// use context value by default
    		return this.context.getWorkerIdKey();
    	else
    		return this.workerIdKey;
    	
    }

    /**
     * @param workerIdKey The {@link Data} key to which worker nodes should write their IDs
     */
    public void setWorkerIdKey(String workerIdKey) {
    	
    	if (this.context == null)	// only set if not already initialized
    		this.workerIdKey = workerIdKey;
    	else
			log.warn("Setting workerIdKey of {} to {} will not have an effect "
					+ "because the process is already initialized",
					this.getId(), workerIdKey);
        
    }

}
