/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import java.util.LinkedList;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.storage.StorageLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.RddSink;
import stream.io.Sink;
import stream.io.multi.MultiStream;
import streams.spark.StreamsSparkContext;

public class BatchExecution implements DProcessExecution {
	
    private transient final static Logger log = LoggerFactory.getLogger(BatchExecution.class);
    
    private DProcessContext context;
    private transient JavaRDD<String> idRdd;
    private transient Sink output;

	@Override
	public void init(DProcessContext context, MultiStream input,
			Sink output) throws Exception {
		
		log.info("Initializing {} for {} workers...",
				context.getProcessId(), input.getStreams().size());
		this.context = context;

        // store sub-stream IDs in an RDD
        List<String> streamIds = new LinkedList<>(input.getStreams().keySet());
        this.idRdd = StreamsSparkContext.sc().parallelize(streamIds, streamIds.size());
        
        this.output = output;
		
	}

	@Override
	public void execute() throws Exception {
		
		log.info("\n\n==== BATCH executor of {} STARTED ====\n", this.context.getProcessId());

        // distribute tasks
		JavaRDD<Data> result = idRdd.flatMap(new BatchTask(this.context))
				.persist(StorageLevel.MEMORY_AND_DISK_SER());
        
        idRdd.unpersist();	// remove input from memory

        // send accumulated results to actual sink
		if (this.output != null && this.output instanceof RddSink) {
			
			log.info("{} forwarding result RDD to sink {}...",
					this.context.getProcessId(), this.output.getId());
			((RddSink) this.output).write(result);
			
		} else {
			
			// always collect results
			log.info("{} collecting results...", this.context.getProcessId());
			List<Data> resultList = result.collect();
			
			if (this.output != null) {
				
				log.info("{} writing {} collected data items to sink {}...",
						this.context.getProcessId(), resultList.size(), this.output.getId());
				this.output.write(resultList);
				
			} else {
				
				log.info("{} collected {} data items not writing to any sink.",
						this.context.getProcessId(), resultList.size());
				
			}
			
		}

        log.info("\n\n==== executor of {} FINISHED ====\n",
                context.getProcessId());
		
	}

}
