/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import java.util.LinkedList;
import java.util.List;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.VoidFunction2;
import org.apache.spark.streaming.Time;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.RddSink;
import stream.io.Sink;
import stream.io.multi.MultiStream;
import streams.spark.StreamsSparkContext;

public class StreamingExecution implements DProcessExecution {
	
    private transient final static Logger log = LoggerFactory.getLogger(StreamingExecution.class);
    
    private DProcessContext context;
	private transient JavaDStream<Data> dataStream;
    private transient Sink output;

	@Override
	public void init(DProcessContext context, MultiStream input, Sink output) throws Exception {
		
		this.context = context;

		// set number of receivers. Max is heuristically set #executors
		int numExecutors = StreamsSparkContext.sc().getConf().getInt("spark.executor.instances", 1);
		int numCores = StreamsSparkContext.sc().getConf().getInt("spark.executor.cores", 2);
		log.info("There are {} executor(s) with {} core(s) each and {} input streams",
				numExecutors, numCores, input.getStreams().size());

		int numReceivers = StreamsSparkContext.getInstance().getNumReceivers();
		if (numReceivers < 1)	// e.g., default "-1"
			numReceivers = numExecutors;
		else
			log.info("Runtime argument 'numreceivers' is set to {}", numReceivers);
		numReceivers = Math.min(numReceivers, input.getStreams().size());	// max number = number of streams
		log.info("Setting numReceivers to {}", numReceivers);

		// partition sub-stream IDs
		List<List<String>> partitions = new LinkedList<>();
		int substreamIndex = 0;
		for (String substreamId : input.getStreams().keySet()) {
			int partitionIndex = substreamIndex % numReceivers;
			if (partitions.size() <= partitionIndex)
				partitions.add(new LinkedList<>());
			partitions.get(partitionIndex).add(substreamId);
			substreamIndex++;
		}

		// create union of multiple receivers (one for each partition)
		LinkedList<JavaDStream<Data>> streamList = new LinkedList<>();
		for (List<String> partition : partitions) {
			JavaDStream<Data> partitionReceiver = StreamsSparkContext.ssc()
					.receiverStream(
							new StreamingReceiver(this.context,
									partition));
			streamList.add(partitionReceiver);
		}
		this.dataStream = StreamsSparkContext.ssc().union(streamList.getFirst(),
						streamList.subList(1, streamList.size()));
		
		this.output = output;
		
	}

	@Override
	public void execute() throws Exception {

		// Let monitor listen to Streaming Context
		final StreamingMonitor monitor = new StreamingMonitor(this.context);
		StreamsSparkContext.ssc().addStreamingListener(monitor);

		// collect processed data from receivers
		this.dataStream.foreachRDD((VoidFunction2<JavaRDD<Data>, Time>) (
				dataRdd, time) -> {

			if (monitor.isProcessRunning()) {

				if (this.output != null && this.output instanceof RddSink) {

					// forward results
				((RddSink) this.output).write(dataRdd);
				log.info(
						"{} forwarded RDD with {} partitions{} to {}.",
						this.context.getProcessId(), dataRdd.partitions()
								.size(), log.isDebugEnabled() ? " and "
								+ dataRdd.count() + " items " : "", this.output
								.getId());

			} else {

				// collect results
				List<Data> resultList = dataRdd.collect();
				dataRdd.unpersist();	// remove from memory
				log.info("{} collected {} items from {} partitions.",
						this.context.getProcessId(), resultList.size(), dataRdd
								.partitions().size());

				// write to sink
				if (this.output != null && resultList.size() > 0)
					this.output.write(resultList);

			}

		} else {

			log.info(
					"{} received batch without running ({} partitions{}).",
					this.context.getProcessId(), dataRdd.partitions().size(),
					log.isDebugEnabled() ? ", " + dataRdd.count() + " items"
							: "");

		}

	})	;

		// start actual computation
		StreamsSparkContext.ssc().start();
		monitor.onProcessStarting(); // some logging

		// check every second, if number of empty RDDs exceeds max or if context
		// is stopped
		while (!StreamsSparkContext.ssc().awaitTerminationOrTimeout(
				StreamsSparkContext.STREAMING_INTERVAL / 2)) {
			if (monitor.isProcessStopped()) {
				log.info("{} STOPPING.", this.context.getProcessId());
				StreamsSparkContext.ssc().stop(false, true);
				break;
			}
		}

		monitor.onProcessFinished();
				
	}

}
