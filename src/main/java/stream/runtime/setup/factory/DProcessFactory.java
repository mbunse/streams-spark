/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime.setup.factory;

import org.w3c.dom.Element;

import stream.runtime.DProcess;
import stream.runtime.DependencyInjection;
import stream.runtime.ProcessContainer;
import stream.runtime.setup.factory.DefaultProcessFactory;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.factory.ProcessConfiguration;
import stream.runtime.setup.factory.ProcessFactory;
import stream.runtime.setup.handler.DProcessElementHandler;
import stream.util.Variables;

/**
 * Factory for creating distributed batch processes.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 * @see stream.runtime.DProcess
 * @see stream.runtime.setup.factory.DefaultProcessFactory
 *
 */
public class DProcessFactory implements ProcessFactory {
			
	private DefaultProcessFactory defaultFactory;
	
	public DProcessFactory(ProcessContainer processContainer,
			ObjectFactory objectFactory, DependencyInjection dependencyInjection) {
		
		// initialize default factory
		this.defaultFactory = new DefaultProcessFactory(processContainer,
				objectFactory, dependencyInjection);
		
	}

	@Override
	public ProcessConfiguration[] createConfigurations(Element e, Variables v) {
		
		// default as starting point
		ProcessConfiguration[] configs = defaultFactory.createConfigurations(e, v);
		
		// set correct process class
		// (tells object factory to create BatchProcess objects)
		for (ProcessConfiguration config : configs) {
			config.setProcessClass(DProcess.class.getCanonicalName());
			config.setProcessType(DProcessElementHandler.ELEMENT_TAG);
		}
		
		return configs;
		
	}

	@Override
	public void createAndRegisterProcesses(ProcessConfiguration[] configs)
			throws Exception {
		
		// default factory does the job
		this.defaultFactory.createAndRegisterProcesses(configs);
		
	}
	
}
