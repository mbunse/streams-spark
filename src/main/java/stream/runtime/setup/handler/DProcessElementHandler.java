/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime.setup.handler;

import org.w3c.dom.Element;

import stream.runtime.DependencyInjection;
import stream.runtime.ElementHandler;
import stream.runtime.ProcessContainer;
import stream.runtime.setup.factory.DProcessFactory;
import stream.runtime.setup.factory.DefaultProcessFactory;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.factory.ProcessConfiguration;
import stream.runtime.setup.factory.ProcessFactory;
import stream.runtime.setup.factory.ProcessorFactory;
import stream.util.Variables;

/**
 * Element handler for the "dProcess" element tag. Will generate instances
 * of the {@link stream.runtime.DProcess} class.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 *
 */
public class DProcessElementHandler implements ElementHandler {

	/**
	 * Tag of the element that is handled by this class.
	 */
	public final static String ELEMENT_TAG = "dProcess";

	protected final ObjectFactory objectFactory;
	protected final ProcessorFactory processorFactory;

	protected final boolean runsOnWorker;
	
	/**
	 * @param objectFactory
	 *            Factory used by the Streams framework for creating objects in
	 *            general
	 * @param processorFactory
	 *            Factory used by the Streams framework for creating processors
	 * @param runsOnWorker
	 *            Must be true if this element handler runs on an executor and
	 *            false, if it runs on the driver. Depending on this parameter,
	 *            either {@link stream.runtime.DProcess} instances are created
	 *            (on the driver, for distributing work), or
	 *            {@link stream.runtime.DefaultProcess} instances are created
	 *            (on executors, for processing a part of distributed data).
	 * @see DefaultProcessFactory#DefaultProcessFactory(ProcessContainer,
	 *      ObjectFactory, DependencyInjection)
	 */
	public DProcessElementHandler(ObjectFactory objectFactory,
			ProcessorFactory processorFactory, boolean runsOnWorker) {
		this.objectFactory = objectFactory;
		this.processorFactory = processorFactory;
		this.runsOnWorker = runsOnWorker;
	}

	@Override
	public String getKey() {
		return ELEMENT_TAG;
	}

	@Override
	public boolean handlesElement(Element element) {
		return element.getNodeName().equalsIgnoreCase(ELEMENT_TAG);
	}

	/**
	 * Depending on {@link #runsOnWorker}, creates a BatchProcess or a
	 * DefaultProcess instance.
	 * 
	 * @see ElementHandler#handleElement(ProcessContainer, Element, Variables,
	 *      DependencyInjection)
	 */
	@Override
	public void handleElement(ProcessContainer container, Element element,
			Variables variables, DependencyInjection dependencies)
			throws Exception {

		ProcessFactory pf;
		if (!this.runsOnWorker) {

			// on the driver, use a BatchProcessFactory / DistributedProcessFactory
			pf = new DProcessFactory(container, objectFactory, dependencies);

		} else {

			// on the worker, use a DefaultProcessFactory
			pf = new DefaultProcessFactory(container, objectFactory, dependencies);

		}

		// start the factory
		ProcessConfiguration[] configs = pf.createConfigurations(element, variables);
		pf.createAndRegisterProcesses(configs);

	}

}
