/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import org.apache.spark.streaming.scheduler.BatchInfo;
import org.apache.spark.streaming.scheduler.ReceiverInfo;
import org.apache.spark.streaming.scheduler.StreamingListener;
import org.apache.spark.streaming.scheduler.StreamingListenerBatchCompleted;
import org.apache.spark.streaming.scheduler.StreamingListenerBatchStarted;
import org.apache.spark.streaming.scheduler.StreamingListenerBatchSubmitted;
import org.apache.spark.streaming.scheduler.StreamingListenerOutputOperationCompleted;
import org.apache.spark.streaming.scheduler.StreamingListenerOutputOperationStarted;
import org.apache.spark.streaming.scheduler.StreamingListenerReceiverError;
import org.apache.spark.streaming.scheduler.StreamingListenerReceiverStarted;
import org.apache.spark.streaming.scheduler.StreamingListenerReceiverStopped;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import streams.spark.StreamsSparkContext;

/**
 * Monitors a {@link DProcess} on Spark Streaming.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 */
public class StreamingMonitor implements StreamingListener {

    private transient static Logger log = LoggerFactory
            .getLogger(StreamingMonitor.class);
    
    private final static boolean STOP_ON_RECEIVER_ERROR = true;

    private final DProcessContext context;
    private final StreamingState state;
    
    private long batchCount = 0;
    private long itemCount = 0;

	/**
	 * @param context
	 *            Information about the process to monitor
	 */
	public StreamingMonitor(DProcessContext context) {
		this.context = context;
		this.state = new StreamingState(context);
	}

    /**
     * Logs some information about the started process.
     */
    public synchronized void onProcessStarting() {
        log.info("\n\n==== STREAMING executor of {} STARTING ====\n"
                        + "BATCH INTERVAL: {}s\n"
                        + "BLOCK INTERVAL: {}\n"
                        + "MAX NUM EMPTY RDDs: {}\n"
                        + "STATE: {}\n"
                        + "\n============================================\n",
                this.context.getProcessId(),
                StreamsSparkContext.STREAMING_INTERVAL,
                StreamsSparkContext.sc().getConf().get("spark.streaming.blockInterval", "200ms"),
                state.getMaxEmptyRdds(),
                this.state.toString());
    }

    /**
     * Logs that the monitored process is finished.
     */
    public synchronized void onProcessFinished() {
        log.info("\n\n==== executor of {} FINISHED ====\n", this.context.getProcessId());
    }

    @Override
    public synchronized void onBatchSubmitted(StreamingListenerBatchSubmitted batchEvent) {
        
    	BatchInfo info = batchEvent.batchInfo();    	
    	    	
    	// update state
        if (!this.state.isStarted() && info.numRecords() > 0) {
        	
        	this.state.markStarted();
            log.info("{} MARKED STARTED.", this.context.getProcessId());
            
        } else if (!this.state.isStarted()) {	// info.numRecords() <= 0
        	
        	log.info("{} WAITING to start.",
                    this.context.getProcessId());
        	
        }
        
    }

    @Override
    public synchronized void onBatchStarted(StreamingListenerBatchStarted batchEvent) {
    	// nothing to do
    }

    @Override
    public synchronized void onBatchCompleted(StreamingListenerBatchCompleted batchEvent) {
    	
    	BatchInfo info = batchEvent.batchInfo();
    	
		// update state
		if (this.state.isStarted() && info.numRecords() <= 0) {
			this.state.increaseNumEmptyRdds();

			// stop because of too many empty RDDs?
			if (this.state.getNumEmptyRdds() > state.getMaxEmptyRdds()
					&& state.getMaxEmptyRdds() >= 0) {
				try {
					this.state.markStopped(false); // regular exit (no failure)
					log.info("{} MARKED STOPPED.", this.context.getProcessId());
				} catch (IllegalStateException e) {	// may occur when marked stopped before
					log.warn("{} was tried to be marked stopped multiple times",
							this.context.getProcessId());
				}
			}
		} else if (info.numRecords() > 0) {
			this.state.zeroEmptyRdds();	// reset counter
		}

        // log info about current batch
        if (this.state.isStarted() && !this.state.isStopped()) {

            this.batchCount++;
            this.itemCount += info.numRecords();

            log.info("\n\n==== executor of {} COMPLETED batch {} ====\n"
                            + "NUM PROCESSED ITEMS: {} (total {})\n"
                            + "TOTAL DELAY: {}ms\n"
                            + "STATE: {}\n"
                            + "\n============================================\n",
                    this.context.getProcessId(),
                    this.batchCount,
                    info.numRecords(),
                    this.itemCount,
                    info.totalDelay().get().toString(),
                    this.state.toString());

        }

    }

    @Override
    public synchronized void onOutputOperationStarted(StreamingListenerOutputOperationStarted arg0) {
        // nothing to do
    }

    @Override
    public synchronized void onOutputOperationCompleted(StreamingListenerOutputOperationCompleted arg0) {
        // nothing to do
    }

    @Override
    public synchronized void onReceiverStarted(StreamingListenerReceiverStarted receiverEvent) {
        log.info("\n\n==== Receiver {} STARTED ====\n"
                        + "PROCESS: {}\n"
                        + "LOCATION: {}\n"
                        + "\n============================================\n",
                receiverEvent.receiverInfo().name(),
                this.context.getProcessId(),
                receiverEvent.receiverInfo().location());
    }

    @Override
    public synchronized void onReceiverError(StreamingListenerReceiverError receiverEvent) {
    	
    	ReceiverInfo info = receiverEvent.receiverInfo();
    	
        log.info("\n\n==== Receiver {} REPORTS ERROR ====\n"
                        + "MESSAGE: {}\n"
                        + "ERROR: {}\n"
                        + "\n============================================\n",
                info.name(),
                info.lastErrorMessage(),
                info.lastError());
        
        if (StreamingMonitor.STOP_ON_RECEIVER_ERROR) {
        	try {
	        	this.state.markStopped(true); // stop with failure status
	        	log.warn("{} MARKED STOPPED due to receiver error: {}",
	        			this.context.getProcessId(),
	        			info.lastErrorMessage());
        	} catch (IllegalStateException e) {	// may occur when marked stopped before
				log.warn("{} was tried to be marked stopped multiple times. "
						+ "There was another receiver error: {}",
						this.context.getProcessId(),
						info.lastErrorMessage());
			}
        }
        
    }

    @Override
    public synchronized void onReceiverStopped(StreamingListenerReceiverStopped receiverEvent) {
        log.info("\n\n==== Receiver {} STOPPED ====\n", receiverEvent.receiverInfo().name());
    }
    
	/**
	 * @return True, iff the associated process state is marked as started.
	 */
	public synchronized boolean isProcessSarted() {
		return this.state.isStarted();
	}

	/**
	 * @return True, iff the associated process state is marked as stopped.
	 */
	public synchronized boolean isProcessStopped() {
		return this.state.isStopped();
	}

	/**
	 * @return True, iff the associated process state is marked as started, but
	 *         not as stopped.
	 */
	public synchronized boolean isProcessRunning() {
		return this.state.isStarted() && !this.state.isStopped();
	}

}
