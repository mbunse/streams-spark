/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import org.apache.spark.broadcast.Broadcast;
import org.w3c.dom.Document;

import stream.Data;
import streams.spark.StreamsSparkContext;

/**
 * Context of a {@link DProcess} that can be sent over the network (e.g.,
 * using spark's broadcasting capabilities). This context holds everything
 * needed by the worker nodes of a distributed process that is required for
 * parallelization.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 *
 */
public class DProcessContext implements java.io.Serializable {

	private static final long serialVersionUID = 2098405847694619399L;
	
	private Document xmlDoc;
	private String processId;
	private String workerIdKey;
	
	/**
	 * @param process The {@link DProcess} this context belongs to
	 */
	public DProcessContext(DProcess process) {
		this.xmlDoc = StreamsSparkContext.getInstance().getXmlDoc();
		this.processId = process.getId();
		this.workerIdKey = process.getWorkerIdKey();
	}
	
	/**
	 * @return A broadcasting handle that, when called inside a worker, will
	 *         transfer this context over the network.
	 */
	public Broadcast<DProcessContext> broadcast() {
		return StreamsSparkContext.getInstance().broadcast(this);
	}
	
	/**
	 * @return The XML document object of the configuration file
	 */
	public Document getXmlDoc() {
		return xmlDoc;
	}

	/**
	 * @return The id of the {@link DProcess} this context belongs to
	 */
	public String getProcessId() {
		return processId;
	}

	/**
	 * @return The {@link Data} key to which worker nodes should write their IDs
	 */
	public String getWorkerIdKey() {
		return workerIdKey;
	}

}
