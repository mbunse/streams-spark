/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.runtime;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Process;
import stream.io.LinkedListSink;
import stream.io.multi.LazySeqMultiStream;
import stream.io.multi.MultiStream;
import stream.runtime.ElementHandler;
import stream.runtime.ProcessContainer;
import stream.runtime.StreamRuntime;
import stream.runtime.setup.factory.ObjectFactory;
import stream.runtime.setup.factory.ProcessorFactory;
import stream.runtime.setup.handler.DProcessElementHandler;
import stream.util.Variables;

/**
 * Worker of a {@link DProcess} that can be run inside a spark task.
 * Using a {@link DProcessContext}, all information this worker needs is
 * provided already.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * @author Karl Stelzner &lt;karl.stelzner@tu-dortmund.de&gt;
 */
public class BatchTask implements FlatMapFunction<String, Data> {

	private final static long serialVersionUID = 6227552219561519374L;
    private transient final static Logger log = LoggerFactory.getLogger(BatchTask.class);
	
	private final DProcessContext context;
    private final LinkedListSink output;

	/**
	 * Creates a worker for the given context and stream.
	 *
	 * @param context
	 *            The context of the process this worker will execute
	 */
    public BatchTask(DProcessContext context) {
        this.context = context;
        this.output = new LinkedListSink();
    }
    
    
    
    @Override
    public Iterator<Data> call(String streamId) throws Exception {

        log.info("\n\n==== Worker started ====\n"
                        + "Process ID: {}\n"
                        + "Stream ID: {}\n"
                        + "\n=========================================\n",
                this.context.getProcessId(),
                streamId);

        ProcessContainer container = this.prepareContainer(streamId, this.output);

        // start worker
        container.execute();
    	container.shutdown();

        return this.output.iterator();
        
    }

	/**
	 * Initializes an instance of the streams framework for this worker for the
	 * given context and stream.
	 * 
	 * @param streamId
	 *            The IDs of the stream to be processed by this worker
	 * @param output
	 *            The {@link LinkedListSink} used to accumulate results at (will
	 *            be stored in a {@link org.apache.spark.rdd.RDD}
	 * @return The execution-ready process container
	 * @throws Exception
	 *             Any exception thrown by initializing the Streams framework
	 */
    public ProcessContainer prepareContainer(String streamId, LinkedListSink output) throws Exception {

        final Variables vars = StreamRuntime.loadUserProperties();

        // custom element handler registration
        final Map<String, ElementHandler> customElementHandlers = new HashMap<>();
        final ObjectFactory objectFactory = ObjectFactory.newInstance();
        final ProcessorFactory processorFactory = new ProcessorFactory(objectFactory);
        customElementHandlers.put(DProcessElementHandler.ELEMENT_TAG,
                new DProcessElementHandler(objectFactory, processorFactory, true));

        ProcessContainer container = new ProcessContainer(this.context.getXmlDoc(), customElementHandlers, vars);
        ProcessContainer.container.remove(container);

        // find process in container and remove all others
        Process process = null;
        for (Process p : container.getProcesses()) {
            if (p.getProperties().get("id").equals(this.context.getProcessId())) {
                process = p;
                break;
            }
        }
        if (process != null) {
        	
            // if sub-streams are specified, then they must exist!
            if (!(process.getInput() instanceof MultiStream)) {
                streamId = null;
            }

            // create sequential MultiStream from sub-stream IDs
            LazySeqMultiStream newInput = new LazySeqMultiStream() {
                @Override
                public Data readNext() throws Exception {
                	try {
                		
                		Data next = super.readNext();
                		if (next != null) {
                            next.put(context.getWorkerIdKey(), "WorkerSource" + next.get(getSourceKey()));
                            return next;
                        } else {
                            return null; // end of stream
                        }
                		
                	} catch (IllegalMonitorStateException e) {
                		log.warn("Underlying stream threw IllegamMonitorStateException."
                				+ "I ignore this and just end reading the stream", e);
                		return null;
                	}
                }
            };

            // set specified sub-streams as input
            if (streamId != null) {
				newInput.addStream(streamId, ((MultiStream) process.getInput())
						.getStreams().get(streamId));
            }

            process.setInput(newInput);

            if (process.getInput() == null)    // check everything is okay
                throw new Exception("Desired stream with id "
                        + newInput.getId() + ":" + streamId
                        + " not found among " + newInput.getStreams().size() + " streams.");

            // connect desired one to context output
            process.setOutput(output);
            container.streams.clear();
            container.streams.put(newInput.getId(), newInput);
            container.getProcesses().clear();
            container.getProcesses().add(process);
            
        } else {
        	
        	throw new IllegalArgumentException("Could not find process with ID " + this.context.getProcessId());
        	
        }
        
        return container;
        
    }
    
}
