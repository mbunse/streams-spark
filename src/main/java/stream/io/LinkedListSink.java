/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io;

import stream.Data;
import stream.io.Sink;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Simple {@link Sink} implementation using a {@link LinkedList}. . Such class
 * is needed, because the other sink implementations do not implement the
 * {@link Iterable} interface, which is required for the output of a
 * SparkStreaming function.
 * 
 * @see org.apache.spark.api.java.function.FlatMapFunction
 * 
 * @author Karl Stelzner &lt;karl.stelzner@tu-dortmund.de&gt;
 */
public class LinkedListSink extends LinkedList<Data> implements Sink {

	private static final long serialVersionUID = -3574297892322638055L;
	
	private String id;

	@Override
	public void init() throws Exception {
		// nothing to do
	}

	@Override
	public boolean write(Data item) throws Exception {
		return this.add(item);
	}

	@Override
	public boolean write(Collection<Data> data) throws Exception {
		return this.addAll(data);
	}

	@Override
	public void close() throws Exception {
		this.clear();
	}
	
	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

}
