/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.rdd.RDD;

import stream.Data;
import stream.io.Sink;

import java.util.List;

/**
 * Marker interface for Sinks that are able to write entire {@link RDD}s of
 * {@link Data} items.
 *
 * @author Mirko Bunse &lt;mirko.bunse@cs.uni-dortmund.de&gt;
 * 
 */
public interface RddSink extends Sink {

	/**
	 * Writes all data items from the RDD into the instance represented by this
	 * sink. This is done within a single transaction.
	 *
	 * @param dataRDD
	 *            An {@link RDD} of {@link Data} items
	 * @return True, iff all data items are written
	 * @throws Exception
	 *             Any exception that may occur during the write
	 */
	public boolean write(JavaRDD<Data> dataRDD) throws Exception;

	/**
	 * Writes a whole list of RDDs.
	 * 
	 * @param dataRDDs
	 *            A list of RDDs
	 * @return True, iff all items of all RDDs are written
	 * @throws Exception
	 *             Any exception that may occur during the write
	 * @see #write(JavaRDD)
	 */
    public boolean write(List<JavaRDD<Data>> dataRDDs) throws Exception;

}
