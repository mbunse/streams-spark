/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io;

import java.util.Collection;

import org.apache.spark.streaming.receiver.Receiver;

import stream.Data;
import stream.io.Sink;

/**
 * Sink which stores incoming {@link Data} items in the given {@link Receiver}.
 * 
 * @author Karl Stelzner &lt;karl.stelzner@tu-dortmund.de&gt;
 * 
 */
public class ReceiverSink implements Sink {
	
	private Receiver<Data> rcv;
	private int streamId;
	
	/**
	 * @param streamId
	 *            The ID of this sink
	 * @param receiver
	 *            The receiver which will be used to store incoming data.
	 */
	public ReceiverSink(int streamId, Receiver<Data> receiver) {
		this.streamId = streamId;
		this.rcv = receiver;
	}

	@Override
	public void init() throws Exception {
		// nothing to do
	}

	@Override
	public boolean write(Data item) throws Exception {
		rcv.store(item);
		return true;
	}

	@Override
	public boolean write(Collection<Data> data) throws Exception {
		rcv.store(data.iterator());
		return true;
	}

	/**
	 * This is called when the connected process is finished. In this case, the
	 * receiver is also stopped.
	 * 
	 * @see stream.io.Sink#close()
	 */
	@Override
	public void close() throws Exception {
		rcv.stop("Worker process finished - stopping receiver " + this.streamId + ".");
	}
	
	@Override
	public String getId() {
		return "" + streamId;
	}

	@Override
	public void setId(String id) {
		throw new UnsupportedOperationException("Should never set ID of ReceiverSink.");
	}

}
