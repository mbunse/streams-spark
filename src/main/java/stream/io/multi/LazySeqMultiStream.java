/*
 *  streams-spark - an Apache Spark Extension for the streams Framework
 *  
 *  Copyright (C) 2016 by the participants of the Project Group 594 at
 *  the University of Technology in Dortmund: Mohamed Asmi, Alexander
 *  Bainczyk, Mirko Bunse, Dennis Gaidel, Michael May, Christian Pfeiffer,
 *  Alexander Schieweck, Lea Schönberger, Karl Stelzner, David Sturm,
 *  Carolin Wiethoff and Lili Xu.
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package stream.io.multi;

import java.util.concurrent.atomic.AtomicBoolean;

import stream.Data;
import stream.io.Stream;
import stream.io.multi.SequentialMultiStream;

/**
 * {@link SequentialMultiStream} which does not initialize sub-streams until
 * they are actually read from.
 * 
 * @author Karl Stelzner &lt;karl.stelzner@tu-dortmund.de&gt;
 * 
 */
public class LazySeqMultiStream extends SequentialMultiStream {
	
	private transient AtomicBoolean closed = new AtomicBoolean(false);

    @Override
    public Data readNext() throws Exception {
    	
        Data data = null;

        while ((data == null && index < additionOrder.size())) {
        	
            String current = additionOrder.get(index);
            log.debug("Current stream is: {}", current);
            Stream currentStream = streams.get(current);
            data = currentStream.read();
            if (this.closed.get()) {
				currentStream.close();
				break;
			}
            if (data != null) {
                data.put(sourceKey, current);
                break;
            }
            currentStream.close();
            log.debug("Stream {} ended, switching to next stream", current);
            
            // close old stream and make sure it's gone
            streams.get(additionOrder.get(index)).close();
            streams.remove(additionOrder.get(index));
            index++;
            if (index >= additionOrder.size()) {
                log.debug("No more streams to read from!");
                return null;
            }
            
            // init next substream
            streams.get(additionOrder.get(index)).init();
            
        }
        
        return data;
        
    }

    @Override
    public void init() throws Exception {
    	
        // initialize only the first sub-stream (if it exists)
        if (streams != null) {
            if (streams.size() > 0) {
                String streamPath = additionOrder.get(0);
                if (streamPath != null) {
                    Stream stream = streams.get(streamPath);
                    if (stream != null) {
                        stream.init();
                    } else {
                        log.error("Stream for {} returned null", streamPath);
                    }
                } else {
                    log.error("Stream path was null");
                }
            } else {
                log.error("No streams to initialize");
            }
        } else {
            log.error("Stream collection was null");
        }

    }

	@Override
	public void close() throws Exception {
		this.closed.set(true);
	}
    
}
