# Directory of logs on the HDFS
EVENTLOG_DIR="${HDFS_BASE}/streams-spark/eventlogs"     # HDFS_BASE is obtained from core-site.xml

# Comma-separated list of dependency jars 
DEPENDENCY_JARS="hdfs:///jars/*.jar"

# Main jar (if not provided, the installed version of streams-spark-core is obtained from maven)
MAIN_JAR="target/streams-spark-0.0.3-SNAPSHOT.jar"



# You could also change defaults of the following properties:
# JOB_NAME="streams-spark"
# ML="false"
# STREAM="false"
# MAXEMPTYRDDS="60"
# NUMRECEIVERS="-1"
# BLOCK_INTERVAL="250ms"
# DRIVER_CORES="2"
# DRIVER_MEMORY="8g"
# MAXRESULTSIZE="8g"
# NUM_EXECUTORS="2"
# EXECUTOR_CORES="2"
# EXECUTOR_MEMORY="4g"
# WAIT_APP_COMPLETION="false"
